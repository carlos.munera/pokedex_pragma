"use strict";

// objecto que contiene la data de los Pokémons
let pokemon = {
  pikachu: {
    name: "Pikachu",
    number: "001",
    Level: "150",
    type: "ELECTRIC",
    hability: "Ray",
    height: "0.5 m",
    weight: "20.5 Kg",
    src: "./images/pikachu.png",
  },
  bulbasaur: {
    name: "Bulbasaur",
    number: "002",
    Level: "80",
    type: "HERB",
    hability: "THICKNESS",
    height: "0.7 m",
    weight: "35.5 Kg",
    src: "./images/bulbasaur.png",
  },
  snorlax: {
    name: "Snorlax",
    number: "003",
    Level: "100",
    type: "NORMAL",
    hability: "GLUTLONY",
    height: "2.1 m",
    weight: "150 Kg",
    src: "./images/snorlax.png",
  },
  psyduck: {
    name: "psyduck",
    number: "004",
    Level: "120",
    type: "WATER",
    hability: "CLOUD NINE",
    height: "0.8 m",
    weight: "19.8 Kg",
    src: "./images/psyduck.png",
  },
};

// carga de valor por defecto
window.onload = function () {
  pokemonSelected("pikachu");
};

// Función que recibe el párametro del botón cliqueado y carga la información del Pokémon
const pokemonSelected = (pokemonName) => {
  let pokemonAttributes = "";
  let pokemonImage =
    '<header class="main-pokemon__header"><i class="fas fa-fire main-pokemon__icon"></i>' +
    '<h2 class="main-pokemon__title">' +
    pokemon[pokemonName].name +
    "</h2></header>" +
    '<img class="main-pokemon__img" src="' +
    pokemon[pokemonName].src +
    '" alt="" />';

  document.getElementById("pokemon").innerHTML = pokemonImage;

  for (let i in pokemon[pokemonName]) {
    if (i !== "src" && i !== "name") {
      pokemonAttributes +=
        '<label class="main-attributes__text">' +
        '<h5 class="main-attributes__title">' +
        i +
        "</h5>" +
        '<h5 class="main-attributes__subtitle"><strong>' +
        pokemon[pokemonName][i] +
        "</strong></h5></label>";
    }

    document.getElementById("attributes").innerHTML = pokemonAttributes;
  }
};
